<?php

namespace Tests\Unit\App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class UserTest extends ModelTestCase
{
    protected function model(): Model
    {
        return new User();
    }

    protected function traits(): array
    {
        $expectedTraits = [
            HasApiTokens::class,
            HasFactory::class,
            Notifiable::class
        ];

        return $expectedTraits;
    }

    protected function fillable(): array
    {
        $expectedFillable = [
            'name',
            'email',
            'password',
        ];

        return $expectedFillable;
    }

    protected function casts(): array
    {
        $expectedCasts = [
            'id' => 'string',
            'email_verified_at' => 'datetime',
        ];

        return $expectedCasts;
    }
}
